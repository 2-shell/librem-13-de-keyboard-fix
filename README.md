# Fix pipe key on *Purism Librem 13* DE laptop keyboards

A bugfix for US keyboards of [Purism Librem 13 laptops](https://puri.sm/products/librem-13/)
broke the `<`/`>`/`|` key of DE keyboard variants. 

According to [Purism's issue tracker](https://tracker.pureos.net/T678) the 
workaround is to revert the mapping adjustments introduced by the  *US keyboard 
bugfix*.

The udev hwdb fragment `librem-13-keyboard.hwdb` in this repository contains the 
corrected mappings. 

Those need to be installed into `/etc/udev/hwdb.d`.

After installation you can either reboot or have systemd rebuild hwdb and
trigger reloading the rules. 

To do all of the above, simply clone this repo, cd into the working copy and 
run: 
```bash
make install
```

