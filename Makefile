HWDB_PRIO=90
HWDB_DIR=/etc/udev/hwdb.d
KBD_INPUT_EVENT=/dev/input/event0


install: librem-13-keyboard.hwdb
	sudo cp $< $(HWDB_DIR)/$(HWDB_PRIO)-$<
	sudo systemd-hwdb -s update
	sudo udevadm trigger $(KBD_INPUT_EVENT)

.PHONY: install
